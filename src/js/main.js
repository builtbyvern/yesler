import $ from 'jquery';
import SmoothScroll from 'smooth-scroll';
import axios from 'axios';
var shuffle = require('shuffle-array')
var parsley = require('parsleyjs')

// initiatie the smooth scroll and ease it on out.
var scroll = new SmoothScroll('a[href*="#"]', {
  speed: 1000,
  easing: 'easeOutQuad'
});

// hide the navbar until things are loaded
$(window).on('load', function() {
  $('#scroll-nav').removeClass('init');
})

// set up the custom navbar
window.onscroll = function () { stickNavBar() };
var navbar = document.getElementById("scroll-nav");
var sticky = navbar.offsetTop;

function stickNavBar() {
  window.pageYOffset >= sticky 
    // is the offset fromt the top?
    ? navbar.classList.add("sticky")
    // if not, unstick it with this class
    : navbar.classList.remove("sticky");
}


// tackle those images
(function () {
  let shuffledArray = [];
  axios.get('logos/logos.json')
    .then((response) => {
      sortLogos(response.data);
    })
    .catch((error) => {
      console.error(error)
    })

  function sortLogos(logos) {
    // randomize array with shuffle and then give us the first 6
    let shuffledArray = shuffle(logos).slice(0, 6)
    const bar = document.getElementById('logos')
    
    // iterate over the 6 and add to the DOM
    shuffledArray.map((logo) => {
      // generate link element and set its attributes
      let link = document.createElement('a');
      link.setAttribute('href', logo.link)
      link.setAttribute('title', logo.title)
      link.setAttribute('target', '_blank')
      
      // generate image and alt tag
      let img = document.createElement('img')
      img.setAttribute('src', `logos/${logo.file}`) 
      img.setAttribute('alt', logo.alt)

      // add image to link
      link.appendChild(img); 

      // add link to log bar
      bar.appendChild(link)
    })
  }
})();



