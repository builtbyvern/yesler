# Yesler Developer Candidate Test

## Installation

Clone the repo, install via npm or yarn and then run either

`npm run start`
 
or 

`yarn start`

to get it up and running locally on a server.



## Notes

* This design doesn't have a footer in the traditional sense so I've opted to name the 
  contact area as a footer

* While bootstrap has built-in helper classes that let us classes more like tachyions.io,
  I find that it's easier to understand that my styles are controlled within my own
  stylesheets so I will rarely be using those.

* The mockup has 'Input' as what looks like a placeholder value but I'm running off the
  assumption that it's there for my knowledge since there are labels above. I'm omitting
  the placeholder because of that.
  
* Font weights: They look bolder than the comp except for webkit when I'm able to render
  them with a good anti-aliasing.

* I've only included jquery for the form error checking as parsley required it. When possible
  I've tried writing my code close to ES6 syntax and then transpiling it with Babel.
